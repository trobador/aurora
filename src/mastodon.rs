pub struct Mastodon<'a> {
    pub client: &'a reqwest::Client,
    pub url: String,
    pub token: String,
}

impl Mastodon<'_> {
    pub async fn post_status(
        &self,
        idempotency_key: String,
        text: String,
    ) -> reqwest::Result<String> {
        let params = [("status", text)];
        self.client
            .post([&self.url, "/api/v1/statuses"].concat())
            .form(&params)
            .header(
                reqwest::header::AUTHORIZATION,
                ["Bearer ", &self.token].concat(),
            )
            .header(
                reqwest::header::HeaderName::from_static("idempotency-key"),
                idempotency_key,
            )
            .send()
            .await?
            .error_for_status()?
            .text()
            .await
    }
}
