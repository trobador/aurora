mod config;
mod current_status;
mod mastodon;
mod site_activity;

use std::env;
use std::path::Path;
use std::process;

use mastodon::Mastodon;
use site_activity::{Activity, SiteActivity};

use log::info;
use quick_xml::de;
use time::format_description::well_known::Iso8601;
use time::OffsetDateTime;
use tokio::fs::File;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn gen_http_client() -> reqwest::Result<reqwest::Client> {
    reqwest::Client::builder()
        .user_agent(config::USER_AGENT)
        .timeout(config::TIMEOUT)
        .build()
}

async fn request_activity(client: &reqwest::Client) -> reqwest::Result<String> {
    client
        .get(config::ENDPOINT)
        .send()
        .await?
        .error_for_status()?
        .text()
        .await
}

fn find_latest_activity<'a>(x: &'a SiteActivity) -> Option<&'a Activity> {
    x.activity.iter().max_by(|x, y| x.datetime.cmp(&y.datetime))
}

async fn post_activity(
    mastodon: &Mastodon<'_>,
    datetime: &OffsetDateTime,
    site_id: &String,
    activity: &Activity,
) -> reqwest::Result<String> {
    let idempotency_key = datetime.unix_timestamp().to_string();
    let datetime_format = time::format_description::parse(
        "[year]-[month]-[day] [hour]:[minute]:[second] UTC[offset_hour sign:mandatory padding:none]",
    ).unwrap();
    let url = "https://aurorawatch.lancs.ac.uk/";
    let message = format!(
        "High geomagnetic activity observed: {} nT\n{} @ {}\n{}",
        activity.value,
        datetime.format(&datetime_format).unwrap(),
        site_id,
        url
    );
    mastodon.post_status(idempotency_key, message).await
}

async fn save_timestamp(datetime: OffsetDateTime) -> Result<()> {
    let mut file = File::create(config::STORE_PATH).await?;
    let iso_datetime = datetime.format(&Iso8601::DATE_TIME_OFFSET)?;

    file.write_all(iso_datetime.as_bytes()).await?;
    file.flush().await?;

    Ok(())
}

async fn load_timestamp() -> Result<OffsetDateTime> {
    if Path::new(config::STORE_PATH).try_exists()? {
        let mut file = File::open(config::STORE_PATH).await?;
        let mut buffer = String::new();

        file.read_to_string(&mut buffer).await?;
        let datetime = OffsetDateTime::parse(&buffer, &Iso8601::DATE_TIME_OFFSET)?;

        Ok(datetime)
    } else {
        Ok(OffsetDateTime::UNIX_EPOCH)
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init();

    let datetime_limit = load_timestamp().await? + config::MAX_FREQUENCY;
    if OffsetDateTime::now_utc() < datetime_limit {
        info!("Early run, exiting...");
        process::exit(0);
    }

    let client = gen_http_client()?;
    let mastodon = Mastodon {
        client: &client,
        url: env::var("MASTODON_URL").expect("MASTODON_URL environment variable"),
        token: env::var("MASTODON_TOKEN").expect("MASTODON_URL environment variable"),
    };

    let xml = request_activity(&client).await?;

    let site_activity = de::from_str::<SiteActivity>(xml.as_str()).unwrap();
    if site_activity.updated.datetime < datetime_limit {
        info!("Recent activity found, exiting...");
        process::exit(0);
    }

    let latest_activity = find_latest_activity(&site_activity).unwrap();
    if latest_activity.status_id == "red" {
        info!("Posting new activity...");
        save_timestamp(site_activity.updated.datetime).await?; // TODO save state
        post_activity(
            &mastodon,
            &site_activity.updated.datetime,
            &site_activity.site_id,
            latest_activity,
        )
        .await?;
    } else {
        info!("Latest status is bellow red, exiting...");
    }

    Ok(())
}
