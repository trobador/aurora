use std::time::Duration;

pub static USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"));
pub const TIMEOUT: Duration = Duration::from_secs(10);
pub const MAX_FREQUENCY: Duration = Duration::from_secs(2 * 60 * 60);
pub const STORE_PATH: &str = "/tmp/aurora_store";
// TODO use site status instead? eg aurorawatch-api.lancs.ac.uk/0.2/status/project/awn/sum-activity.xml
pub const ENDPOINT: &str =
    "https://aurorawatch-api.lancs.ac.uk/0.2/status/alerting-site-activity.xml";
//pub const CURRENT_STATUS_URL: &str =
//    "https://aurorawatch-api.lancs.ac.uk/0.2/status/current-status.xml";
