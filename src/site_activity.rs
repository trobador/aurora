use serde::Deserialize;
use time::OffsetDateTime;

// AuroraWatch API DTD version 0.2.5
// <!ELEMENT site_activity (lower_threshold+,updated,activity+,message*)>

#[derive(Deserialize)]
pub struct LowerThreshold {
    #[serde(rename = "@status_id")]
    pub status_id: String,
    #[serde(rename = "$text")]
    pub text: String,
}

#[derive(Deserialize)]
pub struct Updated {
    #[serde(with = "time::serde::iso8601")]
    pub datetime: OffsetDateTime,
}

#[derive(Deserialize)]
pub struct Activity {
    #[serde(rename = "@status_id")]
    pub status_id: String,
    #[serde(with = "time::serde::iso8601")]
    pub datetime: OffsetDateTime,
    pub value: f64,
}

#[derive(Deserialize)]
pub struct SiteActivity {
    #[serde(rename = "@site_id")]
    pub site_id: String,
    pub lower_threshold: Vec<LowerThreshold>,
    pub updated: Updated,
    pub activity: Vec<Activity>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use quick_xml::de;
    use std::fs;

    #[test]
    fn parse_site_activity() {
        let xml = fs::read_to_string("tests/site-activity.xml").unwrap();
        let site_activity: SiteActivity = de::from_str(xml.as_str()).unwrap();
        assert_eq!(
            site_activity
                .activity
                .iter()
                .find_map(|x| if x.value > 100.0 {
                    Some(x.datetime.to_string())
                } else {
                    None
                })
                .unwrap(),
            "2023-11-27 16:00:00.0 +00:00:00" // reformatted from ISO8601
        );
    }
}
