use serde::Deserialize;
use time::OffsetDateTime;

// AuroraWatch API DTD version 0.2.5
// <!ELEMENT current_status (updated,site_status*,message*)>

#[derive(Deserialize)]
pub struct SiteStatus {
    #[serde(rename = "@project_id")]
    pub project_id: String,
    #[serde(rename = "@site_id")]
    pub site_id: String,
    #[serde(rename = "@site_url")]
    pub site_url: String,
    #[serde(rename = "@status_id")]
    pub status_id: String,
}

#[derive(Deserialize)]
pub struct Updated {
    #[serde(with = "time::serde::iso8601")]
    pub datetime: OffsetDateTime,
}

#[derive(Deserialize)]
pub struct CurrentStatus {
    pub updated: Updated,
    #[serde(default)]
    pub site_status: Vec<SiteStatus>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use quick_xml::de;
    use std::fs;

    #[test]
    fn parse() {
        let xml = fs::read_to_string("tests/current-status.xml").unwrap();
        let status: CurrentStatus = de::from_str(xml.as_str()).unwrap();
        assert_eq!(
            status.updated.datetime.to_string(),
            "2023-11-28 17:36:32.0 +00:00:00" // reformatted from ISO8601
        );
    }
}
