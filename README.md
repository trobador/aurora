# Aurora

This program uses the [AuroraWatch UK API](https://aurorawatch.lancs.ac.uk/api-info/) to check for aurora visibility forecasts and posts alerts to [Mastodon](https://joinmastodon.org/) if there is a chance the aurora will be visible from the UK.

## Requirements

- Rust 2021 (1.56.0+)

## Usage

The program is intended to be run as a cronjob. 

To build:

```
RUSTFLAGS='-C target-feature=+crt-static' cargo build --target x86_64-unknown-linux-gnu --release
```

Uses the following environment variables:

- `MASTODON_URL`: URL of Mastodon instance 
- `MASTODON_TOKEN`: Access token for Mastodon account
- (optional) `RUST_LOG`: Set standard Rust log level

## License

This project is licensed under the AGPLv3 license. See LICENSE for details.

## Contact

Message me at `@trobador@mastodon.social` on Mastodon with any questions!
